# Gestión de citas bibliográficas con Zotero

En un mundo repleto de información, citar apropiadamente es un proceso vital, pero cada vez más complicado, por cuanto se necesita gestionar un alto volumen de documentos. En el mercado existen multitud de gestores bibliográficos, pero la mayoría son de pago. [Zotero](https://www.zotero.org/) es una aplicación open-source que facilita la gestión bibliográfica de una manera rápida y ágil. En esta charla rápida mostraremos sus características, integraciones y ventajas de su uso.

# Bio

Profesor e investigador universitario. Experto en comunicación digital y redes sociales. Ha trabajado en las Universidades de Granada, Jaén y UNIR, así como en medios de comunicación y gabinetes de prensa. Coordinador de comunicación visual y diversos proyectos de [Medialab UGR](https://medialab.ugr.es/). Ha desarrollado diversas líneas de investigación en redes sociales, cultura visual y pensamiento visual, y ha publicado diversos trabajos sobre Instagram y visualización de datos.

# Fechas/horas tentativas

Cualquier jueves-viernes de 12 a 13h.