# Charlas LibreLabGRX

Este es un documento abierto donde puedes proponer charlas para realizar a lo largo del año.

## Instrucciones

* Haz un fork de este repositorio (o edítalo directamente desde la web)
* Crea un documento describiendo tu charla y tu persona.
* Haz un pull request (desde la web lo puedes hacer directamente).
* Espera a que la organización acepte el pull request
* ¡Charla aceptada!


## Charlas propuestas

* [Gestión de citas bibliográficas con Zotero](https://gitlab.com/Prosumidor/charlas-librelabgrx/blob/9bbeded05dad7fcb5e9e50296ac46a3d45e26485/2019-20-1/Charla%20Zotero.md).
* [Visualización de redes sociales con Gephi](https://gitlab.com/Prosumidor/charlas-librelabgrx/blob/9bbeded05dad7fcb5e9e50296ac46a3d45e26485/2019-20-1/Charla%20Gephi.md).
